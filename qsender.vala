public class QSender : Object {

	private Soup.Session session;
	private string base_uri;
	
	public QSender(string in_base_uri) {

		session = new Soup.Session();
		base_uri = in_base_uri;

	}

	public int64 send_msg(string type, int64 id, string msg) throws Error {

		var send_uri = base_uri;
		switch (type) {

		case "private":
			send_uri += "/send_private_msg?user_id=";
			break;

		case "group":
			send_uri += "/send_group_msg?group_id=";
			break;
		}

		send_uri += id.to_string() + "&message=" + msg;

		var soup_message = new Soup.Message("GET", send_uri);

		session.send_message(soup_message);
		
		var parser = new Json.Parser();
		parser.load_from_data((string)soup_message.response_body.data);
		var reader = new Json.Reader(parser.get_root());
		// print((string)soup_message.response_body.data);

		reader.read_member("status");
		var status = reader.get_string_value();
		reader.end_member();

		if (!(status == "ok")) {
			throw new Error(Quark.from_string("Send Fail"), 5700, "Send Fail");
		}
		
		reader.read_member("data");
		reader.read_member("message_id");
		var msg_id = reader.get_int_value();
		reader.end_member();
		reader.end_member();
		
		return msg_id;
			
		;
	}

	public string get_group_name(int64 id) {

		var send_uri = base_uri + "/get_group_info?group_id=" + id.to_string();

		var soup_message = new Soup.Message("GET", send_uri);

		//print("Getting group id for " + id.to_string());
		
		session.send_message(soup_message);

		var parser = new Json.Parser();
		parser.load_from_data((string)soup_message.response_body.data);
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("data");
		reader.read_member("group_name");
		var group_name = reader.get_string_value();
		reader.end_member();
		reader.end_member();

		//print((string)soup_message.response_body.data);
		
		return group_name;

	}

	public string get_self_nickname() {

		var send_uri = base_uri + "/get_login_info";

		var soup_message = new Soup.Message("GET", send_uri);

		//print("Getting group id for " + id.to_string());
		
		session.send_message(soup_message);

		var parser = new Json.Parser();
		parser.load_from_data((string)soup_message.response_body.data);
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("data");
		reader.read_member("nickname");
		var nickname = reader.get_string_value();
		reader.end_member();
		reader.end_member();

		return nickname;
	}

	public int64 get_self_user_id() {

		var send_uri = base_uri + "/get_login_info";

		var soup_message = new Soup.Message("GET", send_uri);

		//print("Getting group id for " + id.to_string());
		
		session.send_message(soup_message);

		var parser = new Json.Parser();
		parser.load_from_data((string)soup_message.response_body.data);
		var reader = new Json.Reader(parser.get_root());

		reader.read_member("data");
		reader.read_member("user_id");
		var user_id = reader.get_int_value();
		reader.end_member();
		reader.end_member();

		return user_id;
	}
		
}