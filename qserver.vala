public class QServer : Soup.Server {

	public Gtk.Stack stack;//传入的要修改的stack
	public GLib.HashTable<string, int64?> chat_list;//存取id的哈希表
	public QSender sender;

	public QServer(Gtk.Stack in_stack) {

		chat_list = new GLib.HashTable<string, int64?>(str_hash, str_equal);
		sender = new QSender("http://127.0.0.1:5700");
		stack = in_stack;
		this.add_handler("/", default_handler);

	}

	//响应HTTP POST请求的主函数
	private static void default_handler(Soup.Server server, Soup.Message msg, string path, GLib.HashTable? query, Soup.ClientContext client) {

		unowned QServer self = server as QServer;

		unowned var chat_list = self.chat_list;
		unowned var stack = self.stack;

		//读取JSON
		var parser = new Json.Parser();

		try {
			parser.load_from_data((string)msg.request_body.data);
		} catch (Error e) {
			print(e.message);
		}
		
		var msg_reader = new Json.Reader(parser.get_root());

		msg_reader.read_member("post_type");
		//判断事件类型，是消息则继续处理
		unowned var post_type = msg_reader.get_string_value();
		msg_reader.end_member();

		switch (post_type) {
		
		case "message":
			//消息处理
			message_handler(server ,msg_reader, stack, chat_list);

		    break;
		default:
			break;
		}
		//HTTP返回值
		msg.set_response("text", Soup.MemoryUse.COPY, "OKAY".data);

	}

	//消息处理函数
	private static bool message_handler(Soup.Server server,Json.Reader msg_reader, Gtk.Stack stack, GLib.HashTable<string, int64?> chat_list) {

		unowned QServer self = server as QServer;
		
		msg_reader.read_member("message_type");
		unowned var msg_type = msg_reader.get_string_value();
		msg_reader.end_member();
		
		int64 chat_id;

		switch (msg_type) {
		case "private":
			msg_reader.read_member("user_id");
			chat_id = msg_reader.get_int_value();
			msg_reader.end_member();
			break;
		case "group":
			msg_reader.read_member("group_id");
			chat_id = msg_reader.get_int_value();
			msg_reader.end_member();
			break;
		default:
			return false;
		}
		
		msg_reader.read_member("raw_message");
		unowned var message = msg_reader.get_string_value();
		msg_reader.end_member();

		msg_reader.read_member("sender");
		msg_reader.read_member("nickname");
		var sender_name = msg_reader.get_string_value();
		msg_reader.end_member();

		msg_reader.read_member("user_id");
		var sender_id = msg_reader.get_int_value();
		msg_reader.end_member();
		msg_reader.end_member();

		//print(message);
		
		var chat_name = msg_type + chat_id.to_string();

		if (chat_list.contains(chat_name)) {//如果已经有聊天，则添加

			//print(chat_name);
			var stack_child = stack.get_child_by_name(chat_name) as Gtk.ScrolledWindow;
			unowned var tmp_viewport = stack_child.get_child() as Gtk.Viewport;
			unowned Gtk.Box chat_box = tmp_viewport.get_child() as Gtk.Box;
			append_message(chat_box, sender_name, sender_id, message);

			//将自动滚动加入事件
			Idle.add(() => {
					 unowned var adj = stack_child.get_vadjustment();
					 adj.set_value(adj.get_upper());
					 //print(adj.get_value().to_string() + "\n");
					 //print(adj.get_upper().to_string() + "\n");
					 return false;
				},Priority.DEFAULT_IDLE);
			
		} else {//无则创建一个聊天
			
			chat_list.insert(chat_name, chat_id);
			var stack_child = new Gtk.ScrolledWindow();
			var chat_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
			stack_child.set_child(chat_box);
			
			switch (msg_type) {
				
			case "private":
				stack.add_titled(stack_child, chat_name, sender_name);
				break;

			case "group":

				var group_name = self.sender.get_group_name(chat_id);
				stack.add_titled(stack_child, chat_name, group_name);
				break;

			default:
				break;
			}
			//填加消息函数
			append_message(chat_box, sender_name, sender_id, message);
			
			//print(chat_name);
			
		}

		return true;
		

	}
	//向一个box对像内添加消息
	private static void append_message(Gtk.Box box, string sender_name, int64 sender_id, string message) {

		var name_label = new Gtk.Label(null);
		name_label.set_markup( "<span foreground=\"#919190\"><small>" + sender_name + "</small></span>");
		name_label.set_halign(Gtk.Align.START);
		
		var msg_label = new Gtk.Label(message);
		msg_label.set_halign(Gtk.Align.START);
		msg_label.set_wrap(true);
		//msg_label.set_valign(Gtk.Align.START);

		box.append(name_label);
		box.append(msg_label);
		
	}
	//发送消息函数
	public bool send_msg(string chat_name, string msg) {

		if (msg == null) {
			return false;
		}

		
		var c = chat_name[0:1];
		//print(c);
		var id = chat_list.lookup(chat_name);
		
		string type;
		
		switch (c) {
		case "p":
			type = "private";
			break;

		default:
			type = "group";
			break;
		}

		try {
			var re = sender.send_msg(type, id, msg);
		} catch (Error e) {
			print("Send Fail");
		}

		var sender_name = sender.get_self_nickname();
		var sender_id = sender.get_self_user_id();
		
		unowned var stack_child = stack.get_child_by_name(chat_name) as Gtk.ScrolledWindow;
		unowned var tmp_viewport = stack_child.get_child() as Gtk.Viewport;
		unowned Gtk.Box chat_box = tmp_viewport.get_child() as Gtk.Box;
		
		append_message(chat_box, sender_name, sender_id, msg);
		
		return true;
	}

}