# Build

Dependencies: gtk4

Build with: meson, ninja, vala, gcc

How to build:
``` shell
cd Gtk-Chat/
meson build && cd build && ninja
```

# Usage

This program listening ```localhost:5701```, config your bot to send http-post to this port.

This program sends http requests to ```localhost:5700```, config your bot to listen on this port.
