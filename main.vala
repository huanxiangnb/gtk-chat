private async void run_qserver(QServer msg_server) {
	
	var loop = new MainLoop();

	
	try {
		msg_server.listen_all(5701, 0);
		yield;
		loop.run();
	} catch (Error e) {
		print(e.message);
	}
	
}

int main(string[] argv) {
	
    Gtk.init();
	var app = new Gtk.Application("cyou.zhaose.chat", GLib.ApplicationFlags.FLAGS_NONE);

	app.activate.connect(() => {
	
			var window = new Gtk.ApplicationWindow(app);
	
			var stack = new Gtk.Stack();
			stack.set_halign(Gtk.Align.FILL);
			stack.set_valign(Gtk.Align.FILL);
			stack.set_hexpand(true);
			stack.set_vexpand(true);

			var stack_sidebar = new Gtk.StackSidebar();

			var start_scroll = new Gtk.ScrolledWindow();
			start_scroll.set_child(stack_sidebar);

			stack_sidebar.set_stack(stack);

			//右界面消息，输入框，发送
			var grid = new Gtk.Grid();
			grid.attach(stack, 0, 0, 2, 1);
			var input = new Gtk.Entry();
			input.set_hexpand(true);
			var send_button = new Gtk.Button.with_label("发送");
			grid.attach(input, 0, 1, 1, 1);
			grid.attach(send_button, 1, 1, 1, 1);

			//主界面左右分屏
			var paned = new Gtk.Paned(Gtk.Orientation.HORIZONTAL);
			paned.set_start_child(start_scroll);
			paned.set_end_child(grid);
			paned.set_position(200);

			//-----------------------
			var msg_server =  new QServer(stack);
			var input_buffer = input.get_buffer();

			send_button.clicked.connect(() => {
					var msg = input_buffer.get_text();
					input_buffer.delete_text(0, -1);
					var chat_name = stack.get_visible_child_name();
					if (!(msg == "")) {
						msg_server.send_msg(chat_name, msg);
					}
				});
			//-----------------

			window.set_default_size(800, 600);

			window.set_child(paned);
			window.set_title("Gtk Chat");

			run_qserver.begin(msg_server);
			
			window.present();

		});

	app.run(argv);
	
	return 0;
}